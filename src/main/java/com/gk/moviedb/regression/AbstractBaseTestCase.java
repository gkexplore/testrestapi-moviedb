package com.gk.moviedb.regression;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Properties;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.gk.driver.RestClient;
import com.gk.moviedb.bean.Config;
import com.gk.moviedb.data.CustomDataProvider;
import com.gk.testdata.TestDataHolder;
import com.gk.testlog.TestLog;
import com.gk.testlog.TestLog.Status;


public class AbstractBaseTestCase {

	public Config endPoint;
	public TestDataHolder testData;
	public CustomDataProvider dataProvider;
	public RestClient client;
	public TestLog testLog;
	
	@Parameters ({"testName"})
	@BeforeTest	
	public void beforeSuite(@Optional("testName")String testName) {
		testLog = new TestLog();
		testLog.setUpLog(System.getProperty("user.dir")+"/Report/"+testName+"_ExtendReport_"+getTimeStamp()+".html");
	}
	
	@BeforeMethod
	public void setUp() {
		
		endPoint = new Config();
		Properties prop = new Properties();
		String env = System.getProperty("env");
		endPoint.setEnv(env);
		System.out.println(String.format(System.getProperty("user.dir")+"/config/application-%s.properties", env));
		try {
			prop.load(new FileInputStream(String.format(System.getProperty("user.dir")+"/config/application-%s.properties", env)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		endPoint.setApiKey("?api_key="+prop.getProperty("apikey"));
		endPoint.setUrl(prop.getProperty("url"));
		endPoint.setSessionID("&session_id="+prop.getProperty("sessionid"));
		client = new RestClient(endPoint.getUrl());
		client.setTestLog(testLog);

	}
	
	@AfterMethod
	public void afterMethod() {
		testLog.log(Status.INFO, "Test Completed Successfully");
		testLog.endTest();
	}
	
	@AfterTest
	public void afterSuite() {
		testLog.close();
	}
	
	public HashMap<String, String> getHeaders(){
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json;charset=utf-8");
		return headers;
	}
	
	public void startTest(String testCaseName) {
		System.out.println(testLog);
		testLog.startTest(testCaseName, endPoint.getEnv());
	}
	
	public String getTimeStamp() {
		Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
		return Long.toString(timeStamp.getTime());
	}
	
}

