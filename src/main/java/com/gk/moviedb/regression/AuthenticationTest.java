package com.gk.moviedb.regression;

import org.testng.annotations.Test;

import com.gk.moviedb.util.RestConstants;

/*
 * This is to validate whether a apikey is working fine after creating it newly
 * https://developers.themoviedb.org/3/authentication
 */
public class AuthenticationTest extends AbstractBaseTestCase
{
	private static final String PATH = "/movie/76341";
	/*
	 * This method is to test if the API key is working
	 */
    @Test
    public void testAPIKey() {
    	
    	 startTest("testAPIKey");
    	 client.get(PATH + endPoint.getApiKey())
    	.expectCode(RestConstants.HTTP_STATUS_CODE_200)
    	.expectMessage(RestConstants.HTTP_STATUS_MESSAGE_OK)
    	.printBody()
    	.expectInBody("Mad Max Collection")
    	.expectInBody("Village Roadshow Pictures");
    	
    }
	
    
}
