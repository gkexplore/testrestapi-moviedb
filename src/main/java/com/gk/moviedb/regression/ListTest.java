package com.gk.moviedb.regression;

import java.util.HashMap;

import org.testng.annotations.Test;

import com.gk.moviedb.data.CustomDataProvider;
import com.gk.moviedb.util.RestConstants;
import com.gk.testlog.TestLog.Status;
import com.google.gson.JsonObject;

/*
 * @author Karthik
 * Create automated test for List feature
 * 
 */
public class ListTest extends AbstractBaseTestCase{
	
	/*
	 * Step 1: Create a movie list
	 * Step 2: Add Movie to a list
	 * Step 3: check if we are able to retrieve list and the newly added movie
	 * Step 4: delete the newly created list
	 */
	//Note: This test will fail and it is a valid failure. See delete section for more details
	@Test(dataProvider = "listAndMedia", dataProviderClass=CustomDataProvider.class)
	public void testAddMovieToList(HashMap<String, String> testData) {
		startTest("testAddMovieToList");
		//create empty list
		int list_id =  client.post("/list"+endPoint.getApiKey()+endPoint.getSessionID(), 
						 	getHeaders(), 
						 	getCreateListRequestBody(testData), 
						 	RestConstants.CONTENT_TYPE_JSON)
						 .expectCode(RestConstants.HTTP_STATUS_CODE_201)
						 .expectMessage(RestConstants.HTTP_STATUS_MESSAGE_CREATED)
						 .printBody()
						 .expectInBody("The item/record was created successfully.")
						 .getIntegerValueFromResponse("list_id");
		
		//add a movie to the newly created list
		client.post("/list/"+list_id+"/add_item"+endPoint.getApiKey()+endPoint.getSessionID(), 
				getHeaders(), 
				getAddMovieToListRequestBody(testData), 
				RestConstants.CONTENT_TYPE_JSON)
			 .expectCode(RestConstants.HTTP_STATUS_CODE_201)
			 .expectMessage(RestConstants.HTTP_STATUS_MESSAGE_CREATED)
			 .printBody()
			 .expectInBody("The item/record was updated successfully.");
		
		//get list and check if the newly add movie is there
		client.get("/list/"+list_id+endPoint.getApiKey()+endPoint.getSessionID())
		.expectCode(RestConstants.HTTP_STATUS_CODE_200)
		.expectMessage(RestConstants.HTTP_STATUS_MESSAGE_OK)
		.printBody()
		.expectInBody("550");
		
		//delete the newly created list
		//There is a bug in the below delete API. It is deleting the newly created list, but returning the below response
		//{"status_code":11,"status_message":"Internal error: Something went wrong, contact TMDb."}
		client.delete("/list/"+list_id+endPoint.getApiKey()+endPoint.getSessionID())
		.printBody()
		.expectCode(RestConstants.HTTP_STATUS_CODE_201);
		
	}
	
	
	/*
	 * Step 1: Create a Empty List
	 * Step 2: Add Movie to the list
	 * Step 3: Check movie is there in the list
	 * Step 4: Delete the newly added movie from the list
	 * Step 5: Check the List is empty now
	 */
	@Test
	public void testDeleteMovieFromList() {
		startTest("testDeleteMovieFromList");
		testLog.log(Status.INFO, "TODO");
	}
	
	/*
	 * Step 1: Create a list
	 * Step 2: check if the movie is already there in the list
	 * Step 3: if the movie is already there in the list, add the new movie
	 */
	@Test
	public void testCheckIfMovieAlreadyExist() {
		startTest("testCheckIfMovieAlreadyExist");
		testLog.log(Status.INFO, "TODO");
	}

	public String getCreateListRequestBody(HashMap<String, String> testData) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("name", testData.get("name"));
		jsonObject.addProperty("description", testData.get("description"));
		jsonObject.addProperty("language", testData.get("language"));
		return jsonObject.toString();
	}
	
	public String getAddMovieToListRequestBody(HashMap<String, String> testData) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("media_id", Integer.parseInt(testData.get("media_id")));
		return jsonObject.toString();
	}
}
