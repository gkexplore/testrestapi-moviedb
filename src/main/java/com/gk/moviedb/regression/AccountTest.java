package com.gk.moviedb.regression;

import java.util.ArrayList;
import java.util.HashMap;

import org.testng.annotations.Test;

import com.gk.moviedb.data.CustomDataProvider;
import com.gk.moviedb.util.RestConstants;
import com.gk.testlog.TestLog.Status;
import com.google.gson.JsonObject;


/*
 * Write all the accounts related test cases here
 * refer - https://developers.themoviedb.org/3/account/
 */
public class AccountTest extends AbstractBaseTestCase {
	
	/*
	 * Check whether we could retrieve private movie/tv list - it should be empty
	 */
	@Test
	public void testGetCreatedList() {
		
		 startTest("testGetCreated");
		
		//get account id
		 String accountID = client.get("/account"+endPoint.getApiKey()+endPoint.getSessionID())
		.expectCode(RestConstants.HTTP_STATUS_CODE_200)
		.expectMessage(RestConstants.HTTP_STATUS_MESSAGE_OK)
		.printBody()
		.getStringValueFromResponse("id");
		
		
		//check if able to retrieve records using accound_id
		 ArrayList<String> createdList = client.get("/account/"+accountID+"/lists"+endPoint.getApiKey()+endPoint.getSessionID())
		.expectCode(RestConstants.HTTP_STATUS_CODE_200)
		.expectMessage(RestConstants.HTTP_STATUS_MESSAGE_OK)
		.printBody()
		.getArrayValueFromResponse("results");
		
		 //assert list is empty as we have not added any list
		 if(createdList.isEmpty()) {
			 testLog.log(Status.PASS, "Expected: Result is empty");
		 }else {
			 testLog.log(Status.FAIL, "Expected: Empty list :: Actual: Result is not empty");
		 }
		
	}
	
	
	//check whether a newly added favorite movie/tv show is added in your movies favorite list
	//this test covers 2 scenarios
	@Test(dataProvider = "movieAndTV", dataProviderClass=CustomDataProvider.class)
	public void testGetFavoriteMoviesAndTV(HashMap<String,String> testData) {
		
		 startTest("testGetFavoriteMoviesAndTV");
		System.out.println("Testing: media_id:"+testData.get("media_id"));
		//get account id
		 String accountID = client.get("/account"+endPoint.getApiKey()+endPoint.getSessionID())
		.expectCode(RestConstants.HTTP_STATUS_CODE_200)
		.expectMessage(RestConstants.HTTP_STATUS_MESSAGE_OK)
		.printBody()
		.getStringValueFromResponse("id");
		 
		 //set favorite movie
		 client.post("/account/"+accountID+"/favorite"+
				 	endPoint.getApiKey()+endPoint.getSessionID(), 
				 	getHeaders(), 
				 	getFavoriteMoviesAndTVRequestBody(testData), 
				 	RestConstants.CONTENT_TYPE_JSON)
				 .expectCode(RestConstants.HTTP_STATUS_CODE_201)
				 .expectMessage(RestConstants.HTTP_STATUS_MESSAGE_CREATED)
				 .printBody()
				 .expectInBody("The item/record was updated successfully.");
		 
		 if(testData.get("media_type").equals("movie")) {
			//check the movie is added in favorite list
			 client.get("/account/"+accountID+"/favorite/movies"+
					 	endPoint.getApiKey()+
					 	endPoint.getSessionID())
					.printBody()
					.expectCode(RestConstants.HTTP_STATUS_CODE_200)
					.expectMessage(RestConstants.HTTP_STATUS_MESSAGE_OK)
					.printBody()
					.expectInBody("\"id\":"+testData.get("media_id"));
		 }else if(testData.get("media_type").equals("tv")){
			//check the tv is added in favorite list
			 client.get("/account/"+accountID+"/favorite/tv"+
					 	endPoint.getApiKey()+
					 	endPoint.getSessionID())
					.printBody()
					.expectCode(RestConstants.HTTP_STATUS_CODE_200)
					.expectMessage(RestConstants.HTTP_STATUS_MESSAGE_OK)
					.printBody()
					.expectInBody("\"id\":"+testData.get("media_id"));
		 }
		 	
	}
	
	
	//TODO
	//check whether we are able to pull rated movie
	//use data provider to pass movie
	//this method can cover a scenario
	@Test
	public void testGetRatedMovies() {
		startTest("testGetRatedMovies");
		testLog.log(Status.SKIP, "TODO");
		//Step 1: rate a movie using -  POST /movie/{movie_id}/rating
			//refer https://developers.themoviedb.org/3/movies/rate-movie
		//Step 2:Use - GET /account/{account_id}/rated/movies
		    //Get a list of all the movies you have rated.
		    //refer https://developers.themoviedb.org/3/account/get-rated-movies
	}
	
	
	//TODO
	//check whether we are able to pull rated tv show
	//use data provider to pass tv show
	//this method can cover a scenario
	@Test
	public void testGetRatedTVShow() {
		startTest("testGetRatedTVShow");
		testLog.log(Status.SKIP, "TODO");
		//Step 1: rate a tv show using -  POST /tv/{tv_id}/rating
			//https://developers.themoviedb.org/3/tv/rate-tv-show
		//Step 2:Use - GET /account/{account_id}/rated/tv
		    //Get a list of all the movies you have rated.
		    //refer https://developers.themoviedb.org/3/account/get-rated-tv-shows
	}
	
	
	//TODO
	//check whether we are able to pull rated tv episode
	//use data provider to pass tv episode
	//this method can cover a scenario
	@Test
	public void testGetRatedTVEpisode() {
		startTest("testGetRatedTVEpisode");
		testLog.log(Status.SKIP, "TODO");
		//Step 1: rate a tv episode using -  POST /tv/{tv_id}/season/{season_number}/episode/{episode_number}/rating
			//https://developers.themoviedb.org/3/tv-episodes/rate-tv-episode
		//Step 2:Use - GET /account/{account_id}/rated/tv/episodes 
			//refer https://developers.themoviedb.org/3/account/get-rated-tv-episodes
	 }
		
	
	
	//TODO
	//add movie/tv show to watchlist
	//check whether they are added
	//use dataprovider to pass values
	//this can cover two test scenarios
	
	@Test
	public void testGetMovieWatchList() {
		startTest("testGetMovieWatchList");
		testLog.log(Status.SKIP, "TODO");
		//Step 1: add movie/tv show to watchlist using - POST /account/{account_id}/watchlist
			//refer https://developers.themoviedb.org/3/account/add-to-watchlist
		//Step 2: Check whether the movie/tv show is added to watchlist using - GET /account/{account_id}/watchlist/movies
			//refer https://developers.themoviedb.org/3/account/get-movie-watchlist
	}
	
	public String getFavoriteMoviesAndTVRequestBody(HashMap<String, String> testData){
		//HashMap<String, String> body = new HashMap<String, String>();
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("media_type", testData.get("media_type"));
		jsonObject.addProperty("media_id", Integer.parseInt(testData.get("media_id")));
		jsonObject.addProperty("favorite", Boolean.valueOf(testData.get("favorite")));
		return jsonObject.toString();
	}

	
}
