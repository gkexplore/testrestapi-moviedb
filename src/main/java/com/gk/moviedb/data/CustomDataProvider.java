package com.gk.moviedb.data;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;

import com.gk.testdata.TestDataHolder;
public class CustomDataProvider {
	
	public TestDataHolder testData;
	

	@DataProvider(name="movieAndTV")
   	public Object[][] getMovieAndTV(Method method){
	   readTestDataFile(method.getName()+".csv");
	   return testData.getAllDataRows();
   	}
	
	@DataProvider(name="listAndMedia")
   	public Object[][] getListAndMedia(Method method){
	   readTestDataFile(method.getName()+".csv");
	   return testData.getAllDataRows();
   	}
	
	public void readTestDataFile(String filePath){
 		Reader reader = null;
		try {
			reader = new InputStreamReader(new FileInputStream(System.getProperty("user.dir")+"/TestData/"+filePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	   if(this.testData== null){
		   this.testData = new TestDataHolder();
	   }
		this.testData.addDataLocation(reader);
   	}
}
