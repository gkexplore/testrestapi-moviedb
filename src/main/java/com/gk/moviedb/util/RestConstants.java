package com.gk.moviedb.util;

public class RestConstants {
	public static final String CONTENT_TYPE_JSON = "application/json";	
	public static final int HTTP_STATUS_CODE_200 = 200;
	public static final int HTTP_STATUS_CODE_201 = 201;
	public static final String HTTP_STATUS_MESSAGE_OK = "OK";
	public static final String HTTP_STATUS_MESSAGE_CREATED = "Created";
}
